//
//  firstPage.swift
//  FrameworkSample
//
//  Created by apple on 07/06/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation
public class firstPage
{
    private var isDebug: Bool!
    
    //2.
    public init() {
        self.isDebug = false
    }
    
    //3.
    public func setup(isDebug: Bool) {
        self.isDebug = isDebug
        print(" firstpage Project is in Debug mode: \(isDebug)")
    }
    
    //4.
    public func YPrint<T>(value: T) {
        if self.isDebug == true {
            print(value)
        } else {
            //Do any stuff for production mode
        }
    }
}
